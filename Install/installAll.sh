#!/bin/bash

user=whoami

if [ ! "$whoami" == "root" ]; then
	echo "please run as root (sudo)"
	echo "exiting script, nothing installed or modified"
else
	apt-get update
	apt-get install python3 --assume-yes
	apt-get install python3-opencv --assume-yes
	apt-get install apache2 --assume-yes
	apt-get install python3-pip --assume-yes
	apt-get install mc --assume-yes
	apt-get install screen --assume-yes
	apt-get install vim --assume-yes
	pip3 install -r mustinstall.txt 
	a2enmod headers
	a2enmod proxy
	a2enmod proxy-http
	a2enmod rewrite
	a2enmod session_cookie
	a2enmod session
	a2enmod socache_shmcb
	a2enmod ssl
	
	#move all files into position
	cp ../Pictures/logo* /var/www/html/
	cp -r ../ControlFiles ~/
	rm /etc/apache2/sites-available/000*
	cp ../ApacheConf/000* /etc/apache2/sites-available/

	#enable camera module
	sed 's/start_x=./start_x=1/' /boot/config.txt >> /boot/config.txt

	#enable run on boot
	declare -i lineNum
	lineNum=$(grep -x -n "exit 0" /etc/rc.local | cut -d: -f1)
	lineNum=$lineNum-1
	sed "$lineNum a /home/pi/ControlFiles/Startservers.sh" /etc/rc.local > /etc/rc.local

	#Create the User password file
	FILE=/etc/apache2/.userhtpasswd
	if [ ! -f "$FILE" ]; then
		echo "Creating htaccess password file"
		echo "Please enter a user name : "
		read username
		htpasswd -c $FILE $username
	else
		echo "A htaccess password file already exists, would you like to add a new user? {y or n}"
		read ans 
		a=0
		while [ $a -eq 0 ]; do

			if [ "$ans" == "y" ]; then
				echo "please enter a username: "
				read username
				htpasswd $FILE $username
				a=1
			elif [ "$ans" == "n" ]; then
				echo "no user being added"
				a=1
			else 
				echo "please enter y for yes or n for no"
			fi
		done
	fi
	echo "Reboot is needed before opperation of the robotic arm"
	echo "Do you want to reboot now? y or n"
	read ans
	while [ "$ans" != "y" || "$ans" != "n"]; do
		echo "Please enter y for yes or n for no"
		read ans
	done
	if [ "$ans" == "y" ]; then
		echo "rebooting Pi now"
		reboot
	else [ $ans == "n" ];
		echo "please reboot to finish the setup and enable the camera module"
	fi
fi