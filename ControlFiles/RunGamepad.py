import json
import flask
from flask import (Flask, render_template, request)
import Coms
import serial
import time
import logging

Coms.setSpeedGlobalVariables()

app = Flask(__name__)
log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)

ser = serial.Serial('/dev/ttyACM0', 115200)
print(ser.name)
time.sleep(1)
speed = 50 
lastTime = time.time()


def controls(key):
     return json.dumps({"key": key})   ####


@app.route('/', methods=['GET'])
def web_control():
    if request.method != "GET":
        return "Invalid method"
    else:
        return render_template('XboxControl.ejs')


@app.route('/api/controls', methods=['POST'])
def controls_api():
    global lastTime
    if request.method != 'POST':
        print ("not POST method")
        return "Invalid method"
    requestJson = flask.request.json
    key = requestJson["key"]
    
    speedStepper = Coms.updateSpdStepper(key[9], key[8], 0.05)
    speedDCMot = Coms.updateSpdDC(key[19], key[18], 0.1)
    linear = 0
    potLid = 0
    elbow = key[3]

    if key[17] == 1:
        linear = key[3]
        elbow = 0
    if key[16] == 1:
        potLid = key[3]
        elbow = 0

    buf = Coms.XboxComsW_Linear_Pot(key[0],elbow,key[1],key[11]-key[10],key[2],linear,potLid,key[4],key[5],key[6],key[7],speedStepper,speedDCMot)
    

    ser.write(buf)
    print(key)    
    #print("%s" %(1/(time.time() - lastTime)))
    lastTime = time.time()

    ser.reset_input_buffer()

    return controls(key)

if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1')
