#Instruction does not have a set number of bytes for the data, but uses the first byte to check the expected number of bytes to receive.
#The last byte is a check sum of the total instruction set.
#   
#     Instruction byte:
#        This byte consists of instructions for which motor, mode, and direction(if applicable)
#              
#        7 6 5 4 3 2 1 0
#         
#        7-4 -- Motor Selection
#         
#        3-2 -- Mode :
#             - 00 -- Absolute Positioning
#             - 01 -- Relative Positioning
#             - 10 -- DC motor movement
#             - 11 -- return all stepper positions
#         
#        1-0 -- Direction (only valid for relative and DC operation)
#             - 00 direction 1 for steppers
#             - 01 direction 2 for steppers
#                  direction 1 for motors
#             - 10 direction 2 for motors  
#           
#       Following bytes
#         For Absolute Positioning :
#             the instruction byte is followed by 3 bytes:
#             0x01 0x02 0x03
#           
#             0x01 -- velocity of the motor [0-255]
#             0x02 -- high byte of the desired position 
#             0x03 -- low byte of the desired position
#        
#       Returning data to the controller
#       The status byte is what is normally returned to the controller, this byte gives information on the status of the read of information and of the 
#
#        Status Byte
#       7 6 5 4 3 2 1 0 
#  
#       2 -- !Commands Valid
#             -- 0 all commands were valid 
#             -- 1 one or more of the commands were invalid    
#       1 -- Motor Motion
#             -- 0 motors are not in motion
#             -- 1 at least one motor is still in motion
#       0 -- Read status of incoming message
#             -- 0 Invalid read of the incoming data ( checksum does not add up to the required value )
#             -- 1 Valid read of the incoming data
import numpy as np
import serial
import time

# Definitions of the motor values
MOTOR_1 = 0x00
MOTOR_2 = 0x10
BASE = 0x00
SHOULDER = 0x10
ELBOW = 0x20
WRIST_AXIAL = 0x40
WRIST_RADIAL = 0x50
AUX_1 = 0x30
AUX_2 = 0x60

LINEAR = AUX_1
POT_LID = AUX_2

MODE_ABS = (0x0 << 2)
MODE_REL = (0x1 << 2)
MODE_DC = (0x2 << 2)
MODE_RETURN = (0x3 << 2)


def sendComs(axPos,axMax,radPos,radMax):
    comByte = 0
    commandBuf = [6,0,0,0,0,0]
    dir = 0;

    axSpd = 255*((axPos-(axMax/2))/(axMax/2))
    
    if (axSpd < 0):
        dir = 1
    if (abs(axSpd) < 30) or (axPos == -1):
        axSpd = 0

    comByte = WRIST_AXIAL + MODE_REL + dir
    commandBuf[1] = int(comByte)
    commandBuf[2] = int(abs(axSpd))

    dir = 0
    comByte = 0

    radSpd = -255*((radPos-(radMax/2))/(radMax/2))
    if (abs(radSpd) < 30) or (radPos == -1):
        radSpd = 0

    if (radSpd < 0):
        dir = 1

    comByte = WRIST_RADIAL + MODE_REL + dir
    commandBuf[3] = int(comByte)
    commandBuf[4] = int(abs(radSpd))
    checkSum = int(sum(commandBuf)) & 0xFF
    commandBuf[5] = checkSum
    Buf = bytes(commandBuf)
    return Buf

def commandByte(axPos, axMax, radPos, radMax, a, f, w, s, e, d, mouseL, mouseR, speed, deadMan):
    buf = []
    #add numBytes as the first entry
    buf.append(0)

    #check if the mouse is in the window and the deadmans switch is pressed
    if (axPos == -1) or (radPos == -1) or (deadMan != 1):
        speed = 0

    if (a^f) == 0:
        spd = 0
    else:
        spd = speed
    
    buf.append(int(BASE + MODE_REL + a))
    buf.append(int(spd/2))

    if (w^s) == 0:
        spd = 0
    else:
        spd = speed
    
    buf.append(int(ELBOW + MODE_REL + s))
    buf.append(int(spd))

    if (e^d) == 0:
        spd = 0
    else:
        spd = speed
    
    buf.append(int(SHOULDER + MODE_REL + e))
    buf.append(int(spd))


    dir = 0
    axSpd = 255*((axPos-(axMax/2))/(axMax/2))
    
    if (axSpd < 0):
        dir = 1
    if (abs(axSpd) < 30) or (axPos == -1) or (deadMan != 1):
        axSpd = 0

    comByte = WRIST_AXIAL + MODE_REL + dir
    buf.append(int(comByte))
    buf.append(int(abs(axSpd)))

    dir = 0
    radSpd = -255*((radPos-(radMax/2))/(radMax/2))

    if (abs(radSpd) < 30) or (radPos == -1) or (deadMan != 1):
        radSpd = 0
    if (radSpd < 0):
        dir = 1
    
    comByte = WRIST_RADIAL + MODE_REL + dir
    buf.append(int(comByte))
    buf.append(int(abs(radSpd)))
    
    #DC motor control
    motspd = 255
    
    
    dir = mouseR + 2*mouseL
    
    if dir > 2:
        motspd = 0

    comByte = MOTOR_1 + MODE_DC + dir
    buf.append(int(comByte))
    buf.append(int(motspd))

    comByte = MOTOR_2 + MODE_DC + dir
    buf.append(int(comByte))
    buf.append(int(motspd))



    buf[0] = int(len(buf) + 1)
    
    #Add the checksum to the end of the array
    buf.append(int(sum(buf)) & 0xFF)

    #Convert it to an array of bytes

    byteBuf = bytes(buf)
    return(byteBuf)




def ElbowBaseComs(baseSigned, elbowSigned):
    comByte = 0
    commandBuf = [6,0,0,0,0,0]
    dir = 0;

    
    
    if (baseSigned > 0):
        dir = 1

    baseSpd = abs(baseSigned)


    comByte =BASE  + MODE_REL + dir
    commandBuf[1] = int(comByte)
    commandBuf[2] = int(baseSpd)

    dir = 0
    comByte = 0

    if (elbowSigned < 0):
        dir = 1

    elbowSpd = abs(elbowSigned)
    
    comByte = ELBOW + MODE_REL + dir
    commandBuf[3] = int(comByte)
    commandBuf[4] = int(elbowSpd)

    checkSum = int(sum(commandBuf)) & 0xFF
    commandBuf[5] = checkSum
    Buf = bytes(commandBuf)
    return Buf

def XboxComs(base, elbow, shoulder, wristRad, wristAx, mot2CCW, mot2CW, mot1CCW, mot1CW, speedStepper, speedDC):
    
    buf = []
    buf.append(0)
    Motor = [BASE , ELBOW, SHOULDER, WRIST_AXIAL, WRIST_RADIAL]
    INPUT = [base, elbow, -shoulder, wristAx, wristRad]

    i = 0
    while i < len(Motor):

        spd = INPUT[i] * 255
        dir = 0
        
        if (spd < 0):
            dir = 1
            spd = abs(spd)
    
        if (spd < 35):
            spd = 35
        spd -= 35

        spd = spd*speedStepper
    
        comByte = Motor[i] + MODE_REL + dir
        buf.append(int(comByte))
        buf.append(int(spd))
        i += 1

    spd = 255 * speedDC
    dir = mot1CCW + 2*mot1CW
    if dir > 2:
        spd = 0

    comByte = MOTOR_1 + MODE_DC + dir
    buf.append(int(comByte))
    buf.append(int(spd))

    spd = 255 * speedDC
    dir = mot2CCW + 2*mot2CW
    if dir > 2:
        spd = 0

    comByte = MOTOR_2 + MODE_DC + dir
    buf.append(int(comByte))
    buf.append(int(spd))

    buf[0] = int(len(buf) + 1)

    buf.append(int(sum(buf)) & 0xFF)
    
    byteBuf = bytes(buf)

    return byteBuf

def setSpeedGlobalVariables():
    global speedStep, butPressUpReadyStep, butPressDownReadyStep
    speedStep = 0.5
    butPressUpReadyStep = 0
    butPressDownReadyStep = 0

    global speedDC, butPressUpReadyDC, butPressDownReadyDC
    speedDC = 1
    butPressUpReadyDC = 0
    butPressDownReadyDC = 0

def updateSpdStepper(up, down, speedInc):
    global speedStep, butPressUpReadyStep, butPressDownReadyStep

    if(up ==1) and (butPressUpReadyStep == 1):
        butPressUpReadyStep = 0
        speedStep += speedInc
        if (speedStep > 1):
            speedStep = 1
    if (up == 0):
        butPressUpReadyStep = 1

    if (down == 1) and (butPressDownReadyStep == 1):
        butPressDownReadyStep = 0
        speedStep -= speedInc
        if (speedStep < 0):
            speedStep = 0
    if (down == 0):
        butPressDownReadyStep = 1

    return speedStep

def updateSpdDC(up, down, speedInc):
    global speedDC, butPressUpReadyDC, butPressDownReadyDC

    if(up ==1) and (butPressUpReadyDC == 1):
        butPressUpReadyDC = 0
        speedDC += speedInc
        if (speedDC > 1):
            speedDC = 1
    if (up == 0):
        butPressUpReadyDC = 1

    if (down == 1) and (butPressDownReadyDC == 1):
        butPressDownReadyDC = 0
        speedDC -= speedInc
        if (speedDC < 0):
            speedDC = 0
    if (down == 0):
        butPressDownReadyDC = 1

    return speedDC

def XboxComsW_Linear_Pot(base, elbow, shoulder, wristRad, wristAx, linear, potLid, mot2CCW, mot2CW, mot1CCW, mot1CW, speedStepper, speedDC):
    
    buf = []
    buf.append(0)
    Motor = [BASE , ELBOW, SHOULDER, WRIST_AXIAL, WRIST_RADIAL, LINEAR, POT_LID]
    INPUT = [base, elbow, -shoulder, wristAx, wristRad, linear, potLid]

    i = 0
    while i < len(Motor):

        spd = INPUT[i] * 255
        dir = 0
        
        if (spd < 0):
            dir = 1
            spd = abs(spd)
    
        if (spd < 35):
            spd = 35
        spd -= 35

        spd = spd*speedStepper
    
        comByte = Motor[i] + MODE_REL + dir
        buf.append(int(comByte))
        buf.append(int(spd))
        i += 1

    spd = 255 * speedDC
    dir = mot1CCW + 2*mot1CW
    if dir > 2:
        spd = 0

    comByte = MOTOR_1 + MODE_DC + dir
    buf.append(int(comByte))
    buf.append(int(spd))

    spd = 255 * speedDC
    dir = mot2CCW + 2*mot2CW
    if dir > 2:
        spd = 0

    comByte = MOTOR_2 + MODE_DC + dir
    buf.append(int(comByte))
    buf.append(int(spd))

    buf[0] = int(len(buf) + 1)

    buf.append(int(sum(buf)) & 0xFF)
    
    byteBuf = bytes(buf)

    return byteBuf


