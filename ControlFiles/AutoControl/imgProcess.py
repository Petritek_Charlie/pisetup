import cv2
import numpy as np

def identifyGreen(img):


    imgHSV = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    g = imgHSV[:,:,1]

#    imgR = img.reshape((-1,3))

#    imgR = np.float32(imgR)

#    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
#    K = 4
#    ret,label,centre = cv2.kmeans(imgR, K, None, criteria, 10,cv2.KMEANS_RANDOM_CENTERS)


#centre = np.uint8(centre)
 #   res = centre[label.flatten()]
 #   res2 = res.reshape((img.shape))

    green = cv2.inRange(g, 140, 255)
    #stVal = cv2.inRange(Sat, 150, 255)
    #anded = cv2.bitwise_and(green,stVal)
    #print(g[320][240], end='      ')
    
    kernel = np.ones((5,5), np.uint8)

    greenBlur = cv2.morphologyEx(green, cv2.MORPH_OPEN, kernel)
    
    #greenBlur = cv2.morphologyEx(greenBlur, cv2.MORPH_CLOSE, kernel)
    diffCentre = (0,0) 
    output = img    

    ret, labels, stats, centroids = cv2.connectedComponentsWithStats(greenBlur)
    if(ret > 0):
        labelAreas = [row[4] for row in stats]

        labelAreas[0] = 0

        maxA = labelAreas.index(max(labelAreas))
        area = stats[maxA][2] * stats[maxA][3]
        areaImg = img.shape[0] * img.shape[1]

        if((stats[maxA][2] > 30) and (stats[maxA][3] > 30) and (stats[maxA][4] > 2000)) and (area < areaImg*0.6):

            topCorner = (stats[maxA][0], stats[maxA][1])
            bottomCorner = (stats[maxA][0]+stats[maxA][2] , stats[maxA][1]+stats[maxA][3])
            
            centreObj = ((int)(stats[maxA][0]+stats[maxA][2]/2) , (int)(stats[maxA][1]+stats[maxA][3]/2))
            centreImg = ((int)(img.shape[1]/2) , (int)(img.shape[0]/2-100))
            
            diffCentre = [centreImg[0] - centreObj[0] , centreImg[1] - centreObj[1]]
            
            #print(diffCentre, end="     ")

            output = cv2.rectangle(img, topCorner, bottomCorner, (0,0,255), 4)
            output = cv2.circle(output, centreObj, 2, (0,0,255), 4)
            output = cv2.circle(output, centreImg, 2, (0,0,255), 4)

    outputComb = (diffCentre ,img)


    return  outputComb


def ProcessImg(img)
    
